angular.module('app.bisseccao', [])
		.controller('bisseccaoController', bisseccaoController); 

function bisseccaoController($scope, bisseccaoService, uteisService) {
	var vm = this;
	vm.tipo = 0;

	vm.calcular= calcular;
	vm.expressao = 'f(x) = ';

	function calcular($event) {

		if (!vm.expressao)
		{
			uteisService.erro('Insira uma expressao', $event);
			return;
		}

		if (!vm.valorIncial)
		{
			uteisService.erro('Insira o valor de (a)', $event);
			return;
		}

		if (!vm.valorFinal)
		{
			uteisService.erro('Insira o valor de (b)', $event);
			return;
		}

		bisseccaoService.tipo = vm.tipo;

		try {
			vm.itens = bisseccaoService.calcular(vm.expressao, vm.valorIncial, vm.valorFinal, vm.epsilon);
		}
		catch(err)
		{	
			vm.itens = [];
			uteisService.erro('Erro na expressão: \n '+err, $event);
		}
	}
}