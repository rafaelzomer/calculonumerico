angular.module('app.bisseccao')
		.service('bisseccaoService', bisseccaoService); 

function bisseccaoService($rootScope, uteisService) {
	var service = {
		calcular: calcular,
		tipo: 0,
		_precisao: _precisao,
	}
	return service;

	function calcular(expressao, a, b, epsilon) {
		var tabela = [],
			k = -1,
			cp = 0,
			funcao = false,
			fa = false,
			fb = false,
			enquanto = false,
			verificaRepeticaoInfinita = 0,
			xkAnterior = 0;
		while(!enquanto)
		{	
			k++;

			a = this._precisao(a);
			b = this._precisao(b);
		

			if (fa == false)
				funcao = math.eval(expressao);
			fa = this._precisao(funcao(a));
			if (fb == false)
				fb = math.eval(expressao);
			fb = this._precisao(funcao(b));

			var xk = (parseFloat(a)+parseFloat(b)) / 2;
			xk = this._precisao(xk);

			funcao = math.eval(expressao);
			fxk = this._precisao(funcao(xk));

			var cp = angular.copy(parseFloat(xk) - parseFloat(a));
			cp = Math.abs(cp);
			cp = this._precisao(cp);



			tabela.push({
				'k': k,
				'a': a,
				'b': b,
				'xk': xk,
				'fa': fa,
				'fb': fb,
				'fxk': fxk,
				'cp': cp
			})



			fa = Number(fa);
			fb = Number(fb);
			fxk = Number(fxk);

			if ((fa < 0 && fb > 0) || (fa > 0 && fb < 0))
			{
				if ((fa < 0 && fxk < 0) || (fa > 0 && fxk > 0))
				{
					a = xk;
					fa = fxk;
				}
				else if ((fb < 0 && fxk < 0) || (fb > 0 && fxk > 0))
				{
					b = xk;
					fb = fxk;
				}
			}
			else
			{
				throw "A expressão não converge";
			}


			enquanto = math.eval(epsilon.replace('cp', cp));

			if (xkAnterior == xk)
			{
				verificaRepeticaoInfinita++;
			}

			if (verificaRepeticaoInfinita >= 1)
			{
				throw "O valor de XK repete-se multiplas vezes";
			}

			xkAnterior = xk;

		}

			return tabela;

	}

	function _precisao(x) {
		if (this.tipo == 0)
			return uteisService.arredondar(x, 5);
		else
			return uteisService.truncar(x, 5);
	}
}