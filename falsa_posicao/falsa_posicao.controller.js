angular.module('app.falsa_posicao', [])
		.controller('falsaPosicaoController', falsaPosicaoController); 

function falsaPosicaoController($scope, $rootScope, falsaPosicaoService, uteisService) {
	var vm = this;
	vm.tipo = 0;

	vm.calcular= calcular;
	if ($rootScope.DEBUG)
	{
		vm.expressao = 'f(x) = x*log(x) - 1';
		vm.valorIncial = '2';
		vm.valorFinal = '3';
		vm.epsilon = 'cp < 10^(-3)';
	}
<<<<<<< HEAD

=======
>>>>>>> a513dce89c5ef6a2c91201a922da999c7adc18d1
	function calcular($event) {
		falsaPosicaoService.tipo = vm.tipo;

		if (!vm.expressao)
		{
			uteisService.erro('Insira uma expressao', $event);
			return;
		}

		if (!vm.valorIncial)
		{
			uteisService.erro('Insira o valor de (a)', $event);
			return;
		}

		if (!vm.valorFinal)
		{
			uteisService.erro('Insira o valor de (b)', $event);
			return;
		}

		if (!vm.epsilon)
		{
			uteisService.erro('Insira o valor de epsilon', $event);
			return;
		}

		try {
			vm.itens = falsaPosicaoService.calcular(vm.expressao, vm.valorIncial, vm.valorFinal, vm.epsilon);
		}
		catch(err)
		{
			vm.itens = [];
			uteisService.erro(''+err, $event);
		}
	}
}