angular.module('app.uteis', [])
		.service('uteisService', uteisService); 

function uteisService($rootScope, $mdDialog) {
	var service = {
		truncar: truncar,
		arredondar: arredondar,
		erro: erro,
	}
	return service;

	function erro(erro, ev) {
	  	$mdDialog.show(
			$mdDialog.alert()
				.clickOutsideToClose(true)
				.title('Atenção')
				.textContent(erro)
				.ok('Ok')
				.targetEvent(ev)
		);
	}

	function truncar(x, digits) {
		x = parseFloat(x);
		var numS = x.toString(),
		decPos = numS.indexOf('.'),
		substrLength = decPos == -1 ? numS.length : 1 + decPos + digits,
		trimmedResult = numS.substr(0, substrLength),
		finalResult = isNaN(trimmedResult) ? 0 : trimmedResult;

		return arredondar(finalResult, digits);
	}

	function arredondar(x, digits) {
		x = parseFloat(x);
		return x.toFixed(digits);
	}
}