angular.module('app.falsa_posicao')
		.service('falsaPosicaoService', falsaPosicaoService); 

function falsaPosicaoService($rootScope, uteisService) {
	var service = {
		calcular: calcular,
		tipo: 0,
		_precisao: _precisao,
	}
	return service;

	function calcular(expressao, a, b, epsilon) {
		var tabela = [],
			k = -1,
			cp = 0,
			funcao = false,
			fa = false,
			fb = false,
			enquanto = false,
			verificaRepeticaoInfinita = 0,
			xkAnterior = 0;
		while(!enquanto)
		{	
			k++;

			a = this._precisao(a);
			b = this._precisao(b);
		

			if (fa == false)
				funcao = math.eval(expressao);
			fa = this._precisao(funcao(a));
			if (fb == false)
				funcao = math.eval(expressao);
			fb = this._precisao(funcao(b));

			var xk = ((a*fb)- (b*fa)) / (fb-fa);
			xk = this._precisao(xk);

			funcao = math.eval(expressao);
			fxk = this._precisao(funcao(xk));

			var cp = angular.copy(fxk);
			cp = Math.abs(cp);
			cp = this._precisao(cp);



			tabela.push({
				'k': k,
				'a': a,
				'b': b,
				'xk': xk,
				'fa': fa,
				'fb': fb,
				'fxk': fxk,
				'cp': cp
			});

<<<<<<< HEAD

			// console.log(k, tabela[tabela.length-1]);

=======
>>>>>>> a513dce89c5ef6a2c91201a922da999c7adc18d1
			fa = Number(fa);
			fb = Number(fb);
			fxk = Number(fxk);

<<<<<<< HEAD
			console.log(fa, fb, fxk)

			if ((fa < 0 && fxk < 0) || (fa > 0 && fxk > 0))
			{
				console.log('subA')
				a = xk;
				fa = fxk;
			}
			else if ((fb < 0 && fxk < 0) || (fb > 0 && fxk > 0))
			{
				console.log('subB')
				b = xk;
				fb = fxk;
=======
			if ((fa < 0 && fb > 0) || (fa > 0 && fb < 0))
			{
				if ((fa < 0 && fxk < 0) || (fa > 0 && fxk > 0))
				{
					a = xk;
					fa = fxk;
				}
				else if ((fb < 0 && fxk < 0) || (fb > 0 && fxk > 0))
				{
					b = xk;
					fb = fxk;
				}
			}
			else
			{
				throw "A expressão não converge";
>>>>>>> a513dce89c5ef6a2c91201a922da999c7adc18d1
			}


			enquanto = math.eval(epsilon.replace('cp', cp));


			if (xkAnterior == xk)
			{
				verificaRepeticaoInfinita++;
			}

			if (verificaRepeticaoInfinita >= 1)
			{
				throw "O valor de XK repete-se multiplas vezes";
			}

			xkAnterior = xk;
		}
	
		return tabela;
	}

	function _precisao(x) {
		if (this.tipo == 0)
			return uteisService.arredondar(x, 5);
		else
			return uteisService.truncar(x, 5);
	}
}