angular.module('app', [
	'ngMaterial',
	'ui.router',
	'md.data.table',
	'app.falsa_posicao',
	'app.bisseccao',
	'app.uteis',
	'app.inicio',
])
.run(run)
.config(config);

function run($rootScope) {
	$rootScope.DEBUG = false;
}

function config($stateProvider, $urlRouterProvider, $mdThemingProvider) {
	$urlRouterProvider.otherwise("/falsa_posicao");

	$stateProvider
		.state('inicio', {
			abstract: true,
			templateUrl: "inicio/inicio.html",
			controller: 'inicioController as vm'
		})
		.state('inicio.falsa_posicao', {
			url: "/falsa_posicao",
			templateUrl: "falsa_posicao/falsa_posicao.html",
			controller: 'falsaPosicaoController as vm',
		})
		.state('inicio.bisseccao', {
			url: "/bisseccao",
			templateUrl: "bisseccao/bisseccao.html",
			controller: 'bisseccaoController as vm',
		})

	$mdThemingProvider.theme('default')
		.primaryPalette('purple')
		.accentPalette('green');
}