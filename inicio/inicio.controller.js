angular.module('app.inicio', [])
		.controller('inicioController', inicioController); 

function inicioController($scope, $mdDialog) {
	var vm = this;
	vm.sobre = sobre;

	function sobre(ev) {
		$mdDialog.show({
			templateUrl: './inicio/sobre.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
			fullscreen: true
		});
	}
}

